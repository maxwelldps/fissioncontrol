import pushover
import requests
import datetime
import json
import subprocess
from time import sleep

class Temp:
	MAX = 55																						# Temp Limits in C
	HYSTERESIS = 5
	HeartBeat = 60																					# Seconds between checks

class Login:
	class splunk:
		host = ''																				    # Splunk Host
		token = ''														                            # Splunk HEC Token

	class IPMI:
		host = ''																				    # IPMI Host
		user = ''																					# IPMI User
		password = ''																		        # IPMI Password
		IPMIEK = ''													                                # IPMI Key

	class healthcheck:
		URL = 'https://hc-ping.com/FFFF'									                        # Healthcheck.io URL or other monitor URL, Posts JSON data Sample - {"temp": {"inlet temp": "19","exhaust temp": "25","temp": "44","temp_1": "35"},"time": "2020-04-29T21:16:27.491872","limits": {"MAX": 55,"HYSTERESIS": 5,"HeartBeat": 60},"AutoCTL": false,"PANIC": false,"Action": "Setting Fan to manual speed ","Trip": false}

	class Pushover:
		APIKey = ''															                        # Pushover APP KEY
		userKey = ''															                    # Pushover User or Dist group Key

class Comms:
	def SplunkIT(data):
		try:
			url = 'https://' + Login.splunk.host + ':8088/services/collector/event'
			authHeader = {'Authorization': 'Splunk ' + Login.splunk.token}
			container = {'event': data}
			requests.post(url, headers=authHeader, json=container, verify=False)
		except:
			Comms.pushMe("PANIC CODE FAIL! -SPLUNK","CODE FAIL PANIC")

	def pushMe(message, title, priority=2,expire=300,retry=30):
		try:
			pushover.init(Login.Pushover.APIKey)
			pushover.Client(Login.Pushover.userKey).send_message(message, title=title,priority=priority,expire=expire,retry=retry)
		except:
			print("SIGNAL: MELTDOWN")																		# I hope you left thermal shoutdown on :/
			ipmi.setSpeed(ipmi.fanCTL.Auto)																	# Last Ditch effort to avoid cHeRnObYl type Meltdown <- F*** tho

	def ETPhoneHOME(mesg):
		try:
			requests.post(Login.healthcheck.URL, data=mesg)
		except:
			Comms.pushMe("PANIC CODE FAIL! - PHONE HOME","CODE FAIL PANIC")

class Logic:
	def CheckTemps(TempData):
		for reading in TempData:
			if int(TempData[reading]) > int(Temp.MAX):
				return {'AutoCTL':True,'Panic':True}
			elif int(TempData[reading]) > (Temp.MAX - Temp.HYSTERESIS):
				return {'AutoCTL':True,'Panic':False}
		return {'AutoCTL':False,'Panic':False}

class ipmi:
	class fanCTL:
			Auto = ['raw','0x30', '0x30', '0x01', '0x01']
			class Manual:
					enable =  ['raw','0x30', '0x30', '0x01', '0x00']
					speedset = ['raw','0x30', '0x30', '0x02', '0xff']
					class speeds:
							default = ['0x12']
							min = ['0x02']
							max = ['0x28']
							S01 = ['0x02']
							S02 = ['0x04']
							S03 = ['0x06']
							S04 = ['0x08']
							S05 = ['0x0A']
							S06 = ['0x0C']
							S07 = ['0x0E']
							S08 = ['0x10']
							S09 = ['0x12']
							S10 = ['0x14']
							S11 = ['0x16']
							S12 = ['0x18']
							S13 = ['0x1A']
							S14 = ['0x1C']
							S15 = ['0x1E']
							S16 = ['0x20']
							S17 = ['0x22']
							S18 = ['0x24']
							S19 = ['0x26']
							S20 = ['0x28']

	def IMPI(CMD):
			if isinstance(CMD, str):
					CMD = CMD.split(' ')
			ConnectIPMI = ["ipmitool", "-I", "lanplus", "-H" , Login.IPMI.host, "-U", Login.IPMI.user, "-P", Login.IPMI.password, "-y", Login.IPMI.IPMIEK]
			ConnectIPMI = ConnectIPMI + CMD
			return subprocess.run(ConnectIPMI, stdout=subprocess.PIPE).stdout.decode('utf-8')

	def getSpeedHEX(speed):
			return ipmi.fanCTL.Manual.speedset + speed

	def setSpeed(Auto, speed='S09'):
			if Auto == True:
					ipmi.IMPI(ipmi.fanCTL.Auto)
			elif  Auto == ipmi.fanCTL.Auto:
					ipmi.IMPI(ipmi.fanCTL.Auto)
			else:
					ipmi.IMPI(ipmi.fanCTL.Manual.enable)
					ipmi.IMPI(ipmi.getSpeedHEX(speed))

	def getTemps():
			IPMIDATA =[]
			zones = []
			temps = {}

			data = ipmi.IMPI('sdr type temperature')

			for line in data.splitlines():											#SPLIT IPMI DATA
					dataobj=[]
					for data in line.split('|'):
							dataobj.append(data.strip())
					IPMIDATA.append(dataobj)
			for data in IPMIDATA:													#Rename Duplacte zones
					if data[0] in zones:
							data[0] = data[0] + '_1'
							zones.append(data[0])
					else:
							zones.append(data[0])

			for data in IPMIDATA:													#return dict
					temps[data[0].lower()] = data[4].split(' ')[0]
			return temps

def run():
	ipmi.setSpeed(ipmi.fanCTL.Auto)													# Put server in Sys fan control

	while True:
		try:
			RawTemps = ipmi.getTemps()												# Pull Temp
			TempCheck = Logic.CheckTemps(RawTemps)
		except:
			print("AHF")
			Comms.pushMe("PANIC CODE FAIL! -mainloop","CODE FAIL PANIC")

		TempPayload = {}
		TempPayload['temp'] = RawTemps
		TempPayload['time'] = datetime.datetime.now().isoformat('T')
		TempPayload['limits'] = {'MAX':Temp.MAX,'HYSTERESIS':Temp.HYSTERESIS,'HeartBeat':Temp.HeartBeat}
		TempPayload['AutoCTL'] = TempCheck['AutoCTL']
		TempPayload['PANIC'] = TempCheck['Panic']

		if TempCheck['AutoCTL'] == True and TempCheck['Panic'] == True:
			TempPayload['Action'] = "PANIC PANIC !!! SETTING AUTO TEMP CTL"
			TempPayload['Trip'] = "Max"
		elif TempCheck['AutoCTL'] == True and TempCheck['Panic'] == False:
			TempPayload['Action'] = "SETTING AUTO TEMP CTL"
			TempPayload['Trip'] = "HYSTERESIS"
		else:
			TempPayload['Action'] = "Setting Fan to manual speed "
			TempPayload['Trip'] = False

		ipmi.setSpeed(TempCheck['AutoCTL'], ipmi.fanCTL.Manual.speeds.default)

		Comms.SplunkIT(TempPayload)													# Send Data to splunk
		print(json.dumps(TempPayload,indent=4))										# Print Latest Message
		Comms.ETPhoneHOME(json.dumps(TempPayload))									# Check into healthcheck.io
		sleep(Temp.HeartBeat)														# Sleeps

run()
