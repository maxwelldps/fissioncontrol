# Fission Control

## Python3 IPMI Fan Control Script for Dell R720

### Features
- [Healthcheck.io](https://healthchecks.io/) intergration
- [Pushover](https://pushover.net/) Intergreation
- [Splunk](https://www.splunk.com/) Intergreation
- JSON DATA post

### Requirements
- [ipmitool](https://github.com/ipmitool/ipmitool)
- [python-pushover](https://pypi.org/project/python-pushover/)

### Splunk data example
![Splunk Example](https://i.ibb.co/WBCtqSY/Capture.png)
![Splunk Example2](https://i.ibb.co/C83xXQN/Capture.png)


### Configure
Besure to set the values below

```python

class Temp:
	MAX = 55																	# Temp Limits in C
	HYSTERESIS = 5
	HeartBeat = 60																# Seconds between checks

class Login:
	class splunk:
		host = ''																# Splunk Host
		token = ''														        # Splunk HEC Token

	class IPMI:
		host = ''																# IPMI Host
		user = ''																# IPMI User
		password = ''															# IPMI Password
		IPMIEK = ''													            # IPMI Key

	class healthcheck:
		URL = ''									                            # Healthcheck.io URL or other monitor URL, Posts JSON data Sample - 

	class Pushover:
		APIKey = ''															    # Pushover APP KEY
		userKey = ''															# Pushover User or Dist group Key
```


Example JSON output
```json
{
    "temp": {
        "inlet temp": "21",
        "exhaust temp": "29",
        "temp": "46",
        "temp_1": "40"
    },
    "time": "2020-04-29T21:33:45.763572",
    "limits": {
        "MAX": 55,
        "HYSTERESIS": 5,
        "HeartBeat": 60
    },
    "AutoCTL": false,
    "PANIC": false,
    "Action": "Setting Fan to manual speed ",
    "Trip": false
}
```
